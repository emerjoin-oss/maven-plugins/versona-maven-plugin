# Versona
A Maven plugin to help managing maven modules versioning.

## Why
Maven version schemes are very powerful and we really admire them. 
The version qualifiers are so beautiful that we can't imagine the world without them.

Maven created the possibilities, what we believe is missing is to create a story on top of those possibilities in a way that
developers can easily version their modules, not having to figure the right qualifier in the process.

Versona is a versioning pattern that leverages from the possibilities already created in maven.

Our understanding is that versioning revolves around the following aspects:
* Target - a definition of the Intended new state. The target state might involve adding new functionalities, patching a bug or vulnerability.
* Maturity - defines how reliable is the module currently, with regards to meeting the defined target. It also defines
the degree of changes that might still happen to accomplish the defined target. As the maturity increases, the lesser
the amount of breaking changes
* Artifact Availability - defines to whom the module artifact is to be made available. 
It can be made available to it's end users or available only within the team that is working on the module
or any other system modules that depends on it.

So how do we do this? 

### The Versona strategy

#### Target definition
We use **Semantic Versioning** to define the **target** state. The target is defined by three unsigned integer values:
* major - version when you make incompatible API changes,
* minor - version when you add functionality in a backwards compatible manner, and
* patch - version when you make backwards compatible bug fixes.

#### Maturity measurement
For maturity measurement we defined **four** levels:
* alfa - figuring things out yet
* beta - doing some tests and some major improvements. Things might change drastically still.
* gama - almost there, making some last minor improvements. No drastic changes are expected at this stage
* stable - target reached, module is reliable and ready for production usage.

The latest levels represent a higher maturity degree, meaning that **stable** > **gama** > **beta** > **alfa**.

#### Artifact availability approaches
It's important to clarify that availability has nothing to deal with maturity. We can decide to
make a non mature version publicly available, for testing or any other form of feedback.
There are two artifact availability modalities:
* public - the artifact is to be made available the final public
* private - the artifact is to be made available to a subset of the final public or another public different from the final public.

Publicly available artifacts are what we call **releases**, while private artifacts are what we call **snapshots**.

All this strategy is implemented by the **Versona** plugin.

### The Versona Plugin
The plugin does one simple thing: It generates a version string and outputs it on the **${package.version}** property.

#### Goals
The **versona** plugin has 2 goals:
* snapshot - generates a snapshot version string. 
* release - generates a release version string. 

##### snapshot
It uses the **SNAPSHOT** Maven qualifier as part of the version string. The whole idea of a 
SNAPSHOT is that it is not a final version. SNAPSHOT artifacts can be updated on the go, as long as the **repository** being used respects supports overwriting. 
A decent Maven **repository** is expected to support overwriting of SNAPSHOT artifacts.

But verona doesn't expect your **repository** to be decent. If you set the property **${package.overwritable}** to **false** then versona will generate a SNAPSHOT version string
that includes the build number. 


##### release
It uses the empty-string Maven qualifier.

### Configurations
The Versona plugin requires the following configurations:
* target - defines the module's target
* maturity - defines the module's maturity

### Other Inputs
* Build number (optional) - The property is **${package.build.number}**. Versona generates the build number automatically using a simple date format: **yyyyMMddHHmmss** by default.


### Guarantees
The Versona plugin offers the following dependency resolution guarantees:
* Release artifacts have higher precedence over snapshot artifacts
* More mature artifacts have higher precedence
* Artifacts generated more recently have higher precedence (achieved via build number)

### Usage
#### Define your maven project version
```xml
<version>${package.version}</version>
```

#### Add the plugin
Here is how your pom should look like:
```xml
 <pluginRepositories>
      <!--Add the emerjoin plugin repository-->
      <pluginRepository>
           <id>bintray-emerjoin-maven</id>
           <name>bintray-plugins</name>
           <url>https://dl.bintray.com/emerjoin/maven</url>
           <snapshots>
               <enabled>false</enabled>
           </snapshots>
      </pluginRepository>
 </pluginRepositories>
    
 <plugins>
    <!--Add the maven plugin-->
    <plugin>
        <groupId>org.emerjoin.maven</groupId>
        <artifactId>versona-maven-plugin</artifactId>
        <version>1.0.0-final</version>
        <configuration>
            <!--Set your current target here-->
             <target>
                 <major>1</major>
                 <minor>0</minor>
                 <patch>0</patch>
             </target>
            <!--Change the maturity as your module evolves-->
             <maturity>alfa</maturity>
        </configuration>
             <executions>
                   <execution>
                       <!--You might want to keep it this way-->
                       <phase>validate</phase>
                       <goals>
                           <!--It's either snapshot or release--> 
                           <goal>snapshot</goal>
                       </goals>
                   </execution>
             </executions>
    </plugin>
</plugins>
```


