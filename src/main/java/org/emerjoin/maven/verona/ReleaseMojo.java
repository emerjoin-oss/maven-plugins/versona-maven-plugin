package org.emerjoin.maven.verona;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;

@Mojo( name = "release",defaultPhase = LifecyclePhase.VALIDATE)
public class ReleaseMojo extends AbstractVersonaMojo {

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        this.applyVersion(makeReleaseVersion());
    }
}
