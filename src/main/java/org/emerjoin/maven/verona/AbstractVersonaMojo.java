package org.emerjoin.maven.verona;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class AbstractVersonaMojo extends AbstractMojo {

    @Parameter(property = "module.target", required = true)
    private SemanticVersion target;

    @Parameter(property = "module.maturity", defaultValue = "alfa")
    private Maturity maturity;

    @Parameter(property = "package.build.number")
    private String buildNumber;

    @Parameter(defaultValue = "${project}",readonly = false, required = true)
    private MavenProject project;

    private static final String OUTPUT_PROPERTY = "package.version";

    protected void applyVersion(String version){
        getLog().info(OUTPUT_PROPERTY+" = "+version);
        project.getProperties ().put(OUTPUT_PROPERTY,
                version);
    }


    protected String makeReleaseVersion(){
        return String.format("%s-%s",target.toString(),maturity.getQualifier().
                getLongForm());
    }


    protected String makeSnapshotVersion(boolean suppressBuildNumber){
        if(buildNumber==null)
            this.buildNumber = makeBuildNumber();
        StringBuilder builder = new StringBuilder();
        builder.append(target.toString());
        builder.append('-');
        if(suppressBuildNumber){
            builder.append(maturity.getQualifier().getLongForm());
        }else{
            builder.append(maturity.getQualifier().getShortForm());
            builder.append(buildNumber);
        }
        builder.append('-');
        builder.append(MavenQualifier.Snapshot.getLongForm());
        return builder.toString();
    }

    private String makeBuildNumber(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
                "yyyyMMddHHmmss");
        return simpleDateFormat.format(new Date());
    }

    public SemanticVersion getTarget() {
        return target;
    }

    public void setTarget(SemanticVersion target) {
        this.target = target;
    }

    public String getBuildNumber() {
        return buildNumber;
    }

    public void setBuildNumber(String buildNumber) {
        this.buildNumber = buildNumber;
    }

    public MavenProject getProject() {
        return project;
    }

    public void setProject(MavenProject project) {
        this.project = project;
    }

    public Maturity getMaturity() {
        return maturity;
    }

    public void setMaturity(Maturity maturity) {
        this.maturity = maturity;
    }
}
