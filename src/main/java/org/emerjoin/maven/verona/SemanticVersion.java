package org.emerjoin.maven.verona;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Parameter;

public class SemanticVersion {

    @Parameter(property = "major",defaultValue = "1")
    private int major;

    @Parameter(property = "minor", defaultValue = "0")
    private int minor;

    @Parameter(property = "patch", defaultValue = "0")
    private int patch;

    public int getMajor() {
        return major;
    }

    public void setMajor(int major) {
        this.major = major;
    }

    public int getMinor() {
        return minor;
    }

    public void setMinor(int minor) {
        this.minor = minor;
    }

    public int getPatch() {
        return patch;
    }

    public void setPatch(int patch) {
        this.patch = patch;
    }

    public String toString(){
        return String.format("%d.%d.%d", major, minor,
                patch);
    }

    void validate() throws MojoExecutionException {
        if(major <0|| minor <0|| patch <0)
            throw new MojoExecutionException("none of the version slots should be negative");
    }

}
