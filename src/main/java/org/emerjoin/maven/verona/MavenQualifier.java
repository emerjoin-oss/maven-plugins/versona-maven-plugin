package org.emerjoin.maven.verona;

public enum MavenQualifier {

    Alfa("a","alfa"),
    Beta("b","beta"),
    Snapshot("SNAPSHOT","SNAPSHOT"),
    ReleaseCandidate("rc","rc"),
    Final("ga","final");

    private String shortForm;
    private String longForm;


    MavenQualifier(String shortForm, String longForm){
        this.shortForm = shortForm;
        this.longForm = longForm;
    }

    public String getShortForm() {
        return shortForm;
    }

    public String getLongForm() {
        return longForm;
    }
}
