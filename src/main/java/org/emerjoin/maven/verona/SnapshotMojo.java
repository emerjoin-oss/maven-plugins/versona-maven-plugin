package org.emerjoin.maven.verona;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

@Mojo( name = "snapshot",defaultPhase = LifecyclePhase.VALIDATE)
public class SnapshotMojo extends AbstractVersonaMojo {

    @Parameter(property = "package.overwritable",defaultValue = "true")
    private boolean overwritable;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        this.applyVersion(makeSnapshotVersion(overwritable));
    }

    public boolean isOverwritable() {
        return overwritable;
    }

    public void setOverwritable(boolean overwritable) {
        this.overwritable = overwritable;
    }
}
