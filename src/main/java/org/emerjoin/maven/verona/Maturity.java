package org.emerjoin.maven.verona;

public enum Maturity {
    alfa(MavenQualifier.Alfa), beta(MavenQualifier.Beta), gama(MavenQualifier.ReleaseCandidate), stable(MavenQualifier.Final);

    private MavenQualifier qualifier;

    Maturity(MavenQualifier qualifier){
        this.qualifier = qualifier;
    }

    public MavenQualifier getQualifier() {
        return qualifier;
    }
}
